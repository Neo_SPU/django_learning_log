"""Defines url patterns for learning_logs."""

from django.urls import path

from . import views

app_name = 'learning_logs'
urlpatterns = [
    # Show Home page
    path('', views.index, name='index'),
    # Show all themes
    path('topics/', views.topics, name='topics'),
    # Show the topic page
    path('topic/<int:topic_id>/', views.topic, name='topic'),
    # Page for add the new them
    path('new_topic/', views.new_topic, name='new_topic'),
    path('new_entry/<int:topic_id>/', views.new_entry, name='new_entry'),
    path('edit_entry/<int:entry_id>/', views.edit_entry, name='edit_entry'),
]