"""learning_log URL Configuration"""

from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('', include('learning_logs.urls')),
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
]
